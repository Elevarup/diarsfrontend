import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Empleado } from "../Modelo/Empleado/Empleado";
import {Response} from "../Modelo/Utils/Response";

@Injectable({
  providedIn: "root"
})
export class EmpleadoService {
  constructor(private http: HttpClient) {}

  url: string = "http://localhost:8080/lunesdiars/empleados";

  ///Obtener una lista de empleados
  getEmpleados(): Observable<Response> {
    return this.http.get<Response>(this.url);
  }

  ///Obtener un empleado por el c[odigo
  getEmpleado(codigo: number): Observable<Response> {
    return this.http.get<Response>(`${this.url}/${codigo}`);
  }

  ///Registrar un empleado
  createEmpleado(empleado: Empleado): Observable<Response> {
    return this.http.post<Response>(this.url, empleado);
  }

  ///Eliminar un empleado
  deleteEmpleado(codigo: number): Observable<Response> {
    return this.http.delete<Response>(`${this.url}/${codigo}`);
  }

  ///Actualizar los datos de un empleado
  updateEmpleado(empleado: Empleado): Observable<Response> {
    return this.http.put<Response>(`${this.url}/${empleado.codigo}`, empleado);
  }
  ///Obtener una lista de empledos, con algunos de sus campos
  getEmpleadoFilter(empleado: Empleado): Observable<Response> {
    return this.http.post<Response>(`${this.url}/parametros`, empleado);
  }
}
