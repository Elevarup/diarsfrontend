import { Component, OnInit } from "@angular/core";
import { PersonaService } from "../../../Service/Cliente/persona.service";
import { Persona } from "../../../Modelo/Cliente/Persona";

import swal from "sweetalert2";

@Component({
  selector: "app-buscarpersona",
  templateUrl: "./buscarpersona.component.html",
  styleUrls: ["./buscarpersona.component.css"]
})
export class BuscarpersonaComponent implements OnInit {
  constructor(private personaService: PersonaService) {}

  ///variables
  listaRetornada: number = 0;
  personas: Persona[];
  persona: Persona = new Persona();

  ngOnInit() {}

  buscar(): void {
    if (
      this.persona.nombre == "" &&
      this.persona.apellidoPaterno == "" &&
      this.persona.apellidoMaterno == ""
    ) {
      swal.fire("Campos vacíos!", "Ingrese al menos un campo", "info");
    } else {
      this.personaService
        .getPersonaByNombreOrApellidoPaternoOrApellidoMaterno(this.persona)
        .subscribe(response => {
          this.personas = response;
          this.listaRetornada = 1;
        });
    }
  }
}
