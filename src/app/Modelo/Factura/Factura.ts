import { Empresa } from "../Cliente/Empresa";
import { Empleado } from "../Empleado/Empleado";

export class Factura {
  codigo: number;
  fecha: Date;
  empleado: Empleado;
  monto: number;

  constructor() {}
}
