import { Factura } from "../Factura/Factura";

export class Empresa {
  codigo: number;
  ruc: string;
  razonSocial: string;
  telefono: string;
  direccion: string;
  correo: string;
  facturas: Factura[];

  constructor() {}
}
