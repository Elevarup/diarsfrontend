import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

///modules
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

///services
import { PersonaService } from "./Service/Cliente/persona.service";
import { EmpresaService } from "./Service/Cliente/empresa.service";
import { ProductoService } from "./Service/producto.service";
import { EmpleadoService } from "./Service/empleado.service";
import { FacturaService } from "./Service/factura.service";

//components
import { HeaderComponent } from "./Component/header/header.component";
import { ClienteheaderComponent } from "./Component/Cliente/clienteheader/clienteheader.component";
import { ListarclienteComponent } from "./Component/Cliente/listarcliente/listarcliente.component";
import { EditarclienteComponent } from "./Component/Cliente/editarcliente/editarcliente.component";
import { BuscarpersonaComponent } from "./Component/Cliente/buscarpersona/buscarpersona.component";
import { EditarempresaComponent } from "./Component/Cliente/editarempresa/editarempresa.component";
import { BuscarempresaComponent } from "./Component/Cliente/buscarempresa/buscarempresa.component";
import { ListarProductosComponent } from "./Component/Producto/listar-productos/listar-productos.component";
import { BuscarProductoComponent } from "./Component/Producto/buscar-producto/buscar-producto.component";
import { EditarProductoComponent } from "./Component/Producto/editar-producto/editar-producto.component";
import { ListarEmpleadosComponent } from "./Component/Empleado/listar-empleados/listar-empleados.component";
import { EditarEmpleadoComponent } from "./Component/Empleado/editar-empleado/editar-empleado.component";
import { BuscarEmpleadoComponent } from "./Component/Empleado/buscar-empleado/buscar-empleado.component";
import { ListarFacturasComponent } from "./Component/Factura/listar-facturas/listar-facturas.component";
import { EditarFacturaComponent } from './Component/Factura/editar-factura/editar-factura.component';
import { DetalleFacturaComponent } from './Component/Factura/detalle-factura/detalle-factura.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './Component/main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ClienteheaderComponent,
    ListarclienteComponent,
    EditarclienteComponent,
    BuscarpersonaComponent,
    EditarempresaComponent,
    BuscarempresaComponent,
    ListarProductosComponent,
    BuscarProductoComponent,
    EditarProductoComponent,
    ListarEmpleadosComponent,
    EditarEmpleadoComponent,
    BuscarEmpleadoComponent,
    ListarFacturasComponent,
    EditarFacturaComponent,
    DetalleFacturaComponent,
    MainNavComponent
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule, BrowserAnimationsModule, LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule],
  providers: [
    PersonaService,
    EmpresaService,
    ProductoService,
    EmpleadoService,
    FacturaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
