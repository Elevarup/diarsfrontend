import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Producto } from "../Modelo/Producto/Producto";

@Injectable({
  providedIn: "root"
})
export class ProductoService {
  constructor(private http: HttpClient) {}

  url: string = "http://localhost:8080/lunesdiars/productos";

  getProductos(): Observable<Producto[]> {
    return this.http.get<Producto[]>(this.url);
  }
  getProducto(producto: Producto): Observable<Producto[]> {
    return this.http.post<Producto[]>(`${this.url}/parametros`, producto);
  }
  getProductoCodigo(codigo: number): Observable<Producto> {
    return this.http.get<Producto>(`${this.url}/${codigo}`);
  }
  createProducto(producto: Producto): Observable<Producto> {
    return this.http.post<Producto>(this.url, producto);
  }
  updateProducto(producto: Producto): Observable<Producto> {
    return this.http.put<Producto>(`${this.url}/${producto.codigo}`, producto);
  }
  deleteProducto(codigo: number): Observable<Producto> {
    return this.http.delete<Producto>(`${this.url}/${codigo}`);
  }
}
