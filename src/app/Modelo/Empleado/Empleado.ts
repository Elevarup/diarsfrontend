export class Empleado {
  codigo: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  documento: string;

  constructor() {}
}
