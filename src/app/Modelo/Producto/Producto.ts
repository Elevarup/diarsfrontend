import { Categoria } from "../Categoria/Categoria";

export class Producto {
  codigo: number;
  nombre: string;
  stock: number;
  precio: number;
  categoria: Categoria;

  constructor() {}
}
