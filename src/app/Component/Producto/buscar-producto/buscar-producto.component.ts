import { Component, OnInit } from "@angular/core";
import { ProductoService } from "../../../Service/producto.service";
import { Producto } from "../../../Modelo/Producto/Producto";

@Component({
  selector: "app-buscar-producto",
  templateUrl: "./buscar-producto.component.html",
  styleUrls: ["./buscar-producto.component.css"]
})
export class BuscarProductoComponent implements OnInit {
  constructor(private productoService: ProductoService) {}

  ngOnInit() {}

  producto: Producto = new Producto();
  productos: Producto[];
  listarRetornada: number = 0;

  buscarProducto(): void {
    this.productoService.getProducto(this.producto).subscribe(response => {
      this.productos = response;
      this.listarRetornada = 1;
    });
  }
}
