import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Categoria } from "../Modelo/Categoria/Categoria";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class CategoriaService {
  constructor(private http: HttpClient) {}
  url: string = "http://localhost:8080/lunesdiars/categorias";

  getCategorias(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(this.url);
  }
}
