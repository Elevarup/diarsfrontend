import { Temporal } from "./Temporal";

export const FACTURAS: Temporal[] = [
  {
    rucEmpresa: "RUCUNO",
    codigoFactura: "1",
    nombreEmpleado: "LuisUno",
    fechaFactura: "2017-01-01"
  },
  {
    rucEmpresa: "RUCUNO",
    codigoFactura: "2",
    nombreEmpleado: "LuisUno",
    fechaFactura: "2017-02-01"
  },
  {
    rucEmpresa: "RUCUNO",
    codigoFactura: "1",
    nombreEmpleado: "LuisDos",
    fechaFactura: "2017-03-01"
  }
];
