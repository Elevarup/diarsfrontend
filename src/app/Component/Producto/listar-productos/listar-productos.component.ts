import { Component, OnInit } from "@angular/core";
import { ProductoService } from "../../../Service/producto.service";
import { Producto } from "../../../Modelo/Producto/Producto";
import { Router } from "@angular/router";

@Component({
  selector: "app-listar-productos",
  templateUrl: "./listar-productos.component.html",
  styleUrls: ["./listar-productos.component.css"]
})
export class ListarProductosComponent implements OnInit {
  constructor(
    private productoService: ProductoService,
    private router: Router
  ) {}

  productos: Producto[];
  producto: Producto = new Producto();
  codigoEditar: number;

  ngOnInit() {
    this.cargarProductos();
  }

  cargarProductos(): void {
    this.productoService.getProductos().subscribe(response => {
      this.productos = response;
    });
  }
  /*
  editar(_producto: Producto): void {
    this.codigoEditar = _producto.codigo;
    alert(`Codigo ${_producto.codigo}`);
    localStorage.setItem("codigoEditar", _producto.codigo.toString());
    this.router.navigate(["/editarproducto"]);
  }
  */
  eliminarProducto(producto: Producto): void {
    this.productoService.deleteProducto(producto.codigo).subscribe(response => {
      alert("Producto eliminado");
      this.productos = this.productos.filter(
        _producto => _producto != producto
      );
    });
  }
}
