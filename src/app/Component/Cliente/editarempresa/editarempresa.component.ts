import { Component, OnInit } from "@angular/core";
import { EmpresaService } from "../../../Service/Cliente/empresa.service";
import { Empresa } from "../../../Modelo/Cliente/Empresa";
import { Router } from "@angular/router";

@Component({
  selector: "app-editarempresa",
  templateUrl: "./editarempresa.component.html",
  styleUrls: ["./editarempresa.component.css"]
})
export class EditarempresaComponent implements OnInit {
  constructor(private empresaService: EmpresaService, private router: Router) {}

  ///variables
  cadena: string = "";
  empresa: Empresa = new Empresa();
  codigo: number;
  ngOnInit() {
    this.cadena = localStorage.getItem("cadenaEmpresa");
    this.codigo = +localStorage.getItem("codigoEmpresa");

    if (this.codigo > 0) this.cargarEmpresa();
  }

  cargarEmpresa() {
    this.empresaService.getEmpresa(this.codigo).subscribe(response => {
      this.empresa = response["empresa"];
    });
  }

  update(): void {
    this.empresaService.updateEmpresa(this.empresa).subscribe(response => {
      alert("Actualización exitoso");
      this.router.navigate(["listarclientes"]);
    });
  }
  create(): void {
    this.empresaService.createEmpresa(this.empresa).subscribe(response => {
      alert("Empresa registrada");
      this.router.navigate(["listarclientes"]);
    });
  }
}
