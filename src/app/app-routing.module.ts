import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ClienteheaderComponent } from "./Component/Cliente/clienteheader/clienteheader.component";
import { ListarclienteComponent } from "./Component/Cliente/listarcliente/listarcliente.component";
//////Componentes de las personas naturales
import { EditarclienteComponent } from "./Component/Cliente/editarcliente/editarcliente.component";
import { BuscarpersonaComponent } from "./Component/Cliente/buscarpersona/buscarpersona.component";
//////Componentes de las empresas
import { EditarempresaComponent } from "./Component/Cliente/editarempresa/editarempresa.component";
import { BuscarempresaComponent } from "./Component/Cliente/buscarempresa/buscarempresa.component";
/////Componentes de los productos
import { ListarProductosComponent } from "./Component/Producto/listar-productos/listar-productos.component";
import { BuscarProductoComponent } from "./Component/Producto/buscar-producto/buscar-producto.component";
import { EditarProductoComponent } from "./Component/Producto/editar-producto/editar-producto.component";
////Componentes de los empleados
import { ListarEmpleadosComponent } from "./Component/Empleado/listar-empleados/listar-empleados.component";
import { EditarEmpleadoComponent } from "./Component/Empleado/editar-empleado/editar-empleado.component";
import { BuscarEmpleadoComponent } from "./Component/Empleado/buscar-empleado/buscar-empleado.component";
////Componentes de las facturas
//import { ListarFacturasComponent } from "./Component/Factura/listar-facturas/listar-facturas.component";
import { ListarFacturasComponent } from "./Component/Factura/listar-facturas/listar-facturas.component";
import {EditarFacturaComponent} from "./Component/Factura/editar-factura/editar-factura.component";
import {DetalleFacturaComponent} from './Component/Factura/detalle-factura/detalle-factura.component';

const routes: Routes = [
  { path: "headercliente", component: ClienteheaderComponent },
  { path: "listarclientes", component: ListarclienteComponent },
  /////Rutas de las personas naturales
  { path: "editarpersona", component: EditarclienteComponent },
  { path: "editarpersona/:codigo", component: EditarclienteComponent },
  { path: "buscarpersona", component: BuscarpersonaComponent },
  ///Rutas de las empresas
  { path: "editarempresa", component: EditarempresaComponent },
  { path: "buscarempresa", component: BuscarempresaComponent },
  ///Rutas de los productos
  { path: "listarproductos", component: ListarProductosComponent },
  { path: "buscarproductos", component: BuscarProductoComponent },
  { path: "agregarproducto/:codigo", component: EditarProductoComponent },
  { path: "editarproducto/:codigo", component: EditarProductoComponent },
  ///Rutas de los empleados
  { path: "listarempleados", component: ListarEmpleadosComponent },
  { path: "editarempleado/:codigo", component: EditarEmpleadoComponent },
  { path: "agregarempleado/:codigo", component: EditarEmpleadoComponent },
  { path: "buscarempleados", component: BuscarEmpleadoComponent },
  ///Rutas de las facturas
  { path: "listarfacturas", component: ListarFacturasComponent },
  { path: "editarfactura/:codigo",component:EditarFacturaComponent},
  {path:"detallefactura/:codigo",component:DetalleFacturaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
