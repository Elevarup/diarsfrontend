export class Persona {
  codigo: number;
  nombre: string = "";
  apellidoPaterno: string = "";
  apellidoMaterno: string = "";
  telefono: string;
  direccion: string;
  correo: string;
  tipoCliente: string;

  constructor() {}
}
