import { Component, OnInit } from "@angular/core";
////Servicios
import { CategoriaService } from "../../../Service/categoria.service";
import { ProductoService } from "../../../Service/producto.service";

///Modelos
import { Categoria } from "../../../Modelo/Categoria/Categoria";
import { Producto } from "../../../Modelo/Producto/Producto";

///Adicionales
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-editar-producto",
  templateUrl: "./editar-producto.component.html",
  styleUrls: ["./editar-producto.component.css"]
})
export class EditarProductoComponent implements OnInit {
  constructor(
    private categoriaService: CategoriaService,
    private productoService: ProductoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargarProducto();
    this.cargarCategorias();
  }
  ///variables locales
  categorias: Categoria[];
  categoria: Categoria = new Categoria();
  producto: Producto = new Producto();
  categoriaSeleccionada: Categoria = new Categoria();
  codigoEditar: number;

  cargarCategorias(): void {
    this.categoriaService.getCategorias().subscribe(response => {
      this.categorias = response;
    });
  }
  cargarProducto(): void {
    this.activatedRoute.params.subscribe(params => {
      this.codigoEditar = params["codigo"];
      if (this.codigoEditar > 0) {
        this.productoService
          .getProductoCodigo(this.codigoEditar)
          .subscribe(response => {
            this.producto = response["producto"];
          });
      }
    });
  }

  agregarProducto(): void {
    this.producto.categoria = this.categoriaSeleccionada;
    this.productoService.createProducto(this.producto).subscribe(response => {
      alert("Producto creado satisfactoriamente");
      this.router.navigate(["/listarproductos"]);
    });
  }
  categoriaEditar(cat1: Categoria, cat2: Categoria): boolean {
    //this.categoriaSeleccionada = this.producto.categoria;

    if (cat1 == null || cat2 == null) return false;
    else cat1.codigo == cat2.codigo;
  }
  editarProducto(): void {
    this.producto.categoria = this.categoriaSeleccionada;
    this.productoService.updateProducto(this.producto).subscribe(response => {
      alert("Producto actualizado");
      this.router.navigate(["listarproductos"]);
    });
  }
}
