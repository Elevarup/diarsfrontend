import { Component, OnInit } from "@angular/core";
import { Empleado } from "../../../Modelo/Empleado/Empleado";
import { EmpleadoService } from "../../../Service/empleado.service";
import { Router, ActivatedRoute } from "@angular/router";
import {Response } from "../../../Modelo/Utils/Response";

@Component({
  selector: "app-editar-empleado",
  templateUrl: "./editar-empleado.component.html",
  styleUrls: ["./editar-empleado.component.css"]
})
export class EditarEmpleadoComponent implements OnInit {
  constructor(
    private empleadoService: EmpleadoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ///Variables
  empleados: Empleado[];
  empleado: Empleado = new Empleado();

 respuesta :Response= new Response();
  
  codigoEditar: number = 0;
  ngOnInit() {
    this.cargarEmpleado();
  }

  cargarEmpleado(): void {
    this.activatedRoute.params.subscribe(params => {
      this.codigoEditar = params["codigo"];
      if (this.codigoEditar > 0) {
        this.empleadoService
          .getEmpleado(this.codigoEditar)
          .subscribe(response => {
            this.respuesta=  response;
            //this.empleado = response["empleado"];
            this.empleado=this.respuesta.entidad;
          });
      }
    });
  }
  editarEmpleado(): void {
    this.empleadoService.updateEmpleado(this.empleado).subscribe(response => {
      alert("Empleado actualizado");
      this.router.navigate(["/listarempleados"]);
    });
  }
  agregarEmpleado(): void {
    this.empleadoService.createEmpleado(this.empleado).subscribe(response => {
      alert("Producto agregado satisfactoriamente");
      this.router.navigate(["/listarempleados"]);
    });
  }
}
