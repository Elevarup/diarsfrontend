import { Component, OnInit } from "@angular/core";
import { Empresa } from "../../../Modelo/Cliente/Empresa";
import { EmpresaService } from "../../../Service/Cliente/empresa.service";
import swal from "sweetalert2";

@Component({
  selector: "app-buscarempresa",
  templateUrl: "./buscarempresa.component.html",
  styleUrls: ["./buscarempresa.component.css"]
})
export class BuscarempresaComponent implements OnInit {
  constructor(private empresaService: EmpresaService) {}

  ///variables
  listaRetornada: number = 0;
  ////Objetos empresa
  empresa: Empresa = new Empresa();
  empresas: Empresa[];

  ngOnInit() {}

  buscar(): void {
    if (this.empresa.ruc == "" && this.empresa.razonSocial == "") {
      swal.fire("Campos vacíos!", "Ingrese al menos un campo", "info");
    } else {
      this.empresaService
        .findByRucOrRazonSocial(this.empresa)
        .subscribe(response => {
          this.empresas = response;
          this.listaRetornada = 1;
        });
    }
  }
  agregar():void{}
}
