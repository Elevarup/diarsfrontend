import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import swal from "sweetalert2";

//Modelo
import { Persona } from "../../../Modelo/Cliente/Persona";

//Service
import { PersonaService } from "../../../Service/Cliente/persona.service";

@Component({
  selector: "app-editarcliente",
  templateUrl: "./editarcliente.component.html",
  styleUrls: ["./editarcliente.component.css"]
})
export class EditarclienteComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private personaService: PersonaService
  ) {}

  ///Clientes
  persona: Persona = new Persona();
  ///variables
  accion: string = "editar";
  cadena: string;

  ///Inicializador
  ngOnInit() {
    this.cadena = localStorage.getItem("cadenaPersona");
    let codigo = localStorage.getItem("codigoPersona");
    console.log("OnInt,cadena: ", this.cadena.toString());
    console.log("OnInit,codigo: ", codigo);
    if (this.cadena == "lleno") this.cargarCliente();
  }
  //////////////////////////
  /////////////

  cargarCliente(): void {
    let codigo = +localStorage.getItem("codigo");
    console.log("CargarCliente, codigo: ", codigo);
    this.personaService
      .getPersona(codigo)
      .subscribe(response => (this.persona = response["persona"]));
  }
  update(_persona: Persona): void {
    this.personaService.update(_persona).subscribe(response => {
      swal.fire({
        position: "top-end",
        icon: "success",
        title: "Actualización exitosa",
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigate(["listarclientes"]);
    });
  }
  agregar(_persona: Persona): void {
    this.personaService.create(_persona).subscribe(response => {
      swal.fire({
        position: "center",
        icon: "success",
        title: "Registro exitoso",
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigate(["listarclientes"]);
    });
  }
}
