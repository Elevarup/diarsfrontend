import { Component, OnInit } from "@angular/core";
import { Persona } from "../../../Modelo/Cliente/Persona";
import { Empresa } from "../../../Modelo/Cliente/Empresa";
import { PersonaService } from "../../../Service/Cliente/persona.service";
import { EmpresaService } from "../../../Service/Cliente/empresa.service";

import { Router } from "@angular/router";

import swal from "sweetalert2";
@Component({
  selector: "app-listarcliente",
  templateUrl: "./listarcliente.component.html",
  styleUrls: ["./listarcliente.component.css"]
})
export class ListarclienteComponent implements OnInit {
  constructor(
    private personaService: PersonaService,
    private empresaService: EmpresaService,
    private router: Router
  ) {}

  ////Variables
  tipoCliente: number = 1;
  tituloCliente: string = "";
  ////Arreglos
  personas: Persona[];
  empresas: Empresa[];

  ngOnInit() {
    this.listar();
  }

  cambiarTipoCliente(): void {
    this.tipoCliente = this.tipoCliente * -1;
    this.listar();
  }

  listar(): void {
    if (this.tipoCliente == 1) {
      this.tituloCliente = "LISTA DE PERSONAS NATURALES";
      this.personaService.getPersonas().subscribe(personas => {
        this.personas = personas;
      });
    } else if (this.tipoCliente == -1) {
      this.tituloCliente = "LISTA DE EMPRESAS";
      this.empresaService.getEmpresas().subscribe(response => {
        this.empresas = response;
      });
    }
  }

  eliminar(persona: Persona): void {
    swal
      .fire({
        title: "¿Seguro que desea eliminar el registro?",
        text: `¿Estás seguro que deseas eliminar cliente ${persona.nombre} ${persona.apellidoPaterno}`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, eliminar el registro"
      })
      .then(result => {
        if (result.value) {
          this.personaService.delete(persona.codigo).subscribe(response => {
            this.personas = this.personas.filter(
              _persona => _persona !== persona
            );
            swal.fire(
              "Eliminado!",
              "El cliente fue eliminado con éxito",
              "success"
            );
            this.router.navigate(["listarclientes"]);
          });
        }
      });
  }
  agregarCliente(cadena: string, _codigo: number): void {
    if (this.tipoCliente == 1) {
      localStorage.setItem("cadenaPersona", cadena);
      localStorage.setItem("codigoPersona", _codigo.toString());
      this.router.navigate(["editarpersona"]);
    } else if (this.tipoCliente == -1) {
      localStorage.setItem("cadenaEmpresa", cadena);
      localStorage.setItem("codigoEmpresa", _codigo.toString());
      this.router.navigate(["editarempresa"]);
    }
  }
  buscarCliente(): void {
    if (this.tipoCliente == 1) this.router.navigate(["buscarpersona"]);
    else if (this.tipoCliente == -1) this.router.navigate(["buscarempresa"]);
  }
}
