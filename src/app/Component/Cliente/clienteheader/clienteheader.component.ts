import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-clienteheader",
  templateUrl: "./clienteheader.component.html",
  styleUrls: ["./clienteheader.component.css"]
})
export class ClienteheaderComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  pantallaListar(): void {
    this.router.navigate(["listarclientes"]);
  }
}
