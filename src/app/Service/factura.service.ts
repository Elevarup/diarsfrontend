import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Factura } from "../Modelo/Factura/Factura";
import {Response} from "../Modelo/Utils/Response"; 

@Injectable({
  providedIn: "root"
})
export class FacturaService {
  constructor(private http: HttpClient) {}

  url: string = "http://localhost:8080/lunesdiars/facturas";

  getFacturas(): Observable<Response> {
    return this.http.get<Response>(this.url);
  }
  createFactura(factura: Factura): Observable<Factura> {
    return this.http.post<Factura>(this.url, factura);
  }
  updateFactura(factura: Factura): Observable<Factura> {
    return this.http.put<Factura>(`${this.url}/${factura.codigo}`, factura);
  }
  deleteFactura(codigo: number): Observable<Response> {
    return this.http.delete<Response>(`${this.url}/${codigo}`);
  }
  getFacturaFiltro(factura: Factura): Observable<Factura[]> {
    return this.http.post<Factura[]>(`${this.url}/parametros`, factura);
  }

  getFactura(id_factura:number):Observable<Response>{
    return this.http.get<Response>(`${this.url}/${id_factura}`);
  }
}
