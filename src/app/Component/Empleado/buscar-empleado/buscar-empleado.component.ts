import { Component, OnInit } from "@angular/core";
import { Empleado } from "../../../Modelo/Empleado/Empleado";
import { EmpleadoService } from "../../../Service/empleado.service";
import { Router } from "@angular/router";
import {Response } from "../../../Modelo/Utils/Response";

@Component({
  selector: "app-buscar-empleado",
  templateUrl: "./buscar-empleado.component.html",
  styleUrls: ["./buscar-empleado.component.css"]
})
export class BuscarEmpleadoComponent implements OnInit {
  constructor(
    private empleadoService: EmpleadoService,
    private router: Router
  ) {}

  ///Variables
  empleados: Empleado[];
  empleado: Empleado = new Empleado();
  listarRetornada: number = 0;

  respuesta: Response = new Response();
  ngOnInit() {}

  buscarEmpleado(): void {
    this.empleadoService
      .getEmpleadoFilter(this.empleado)
      .subscribe(response => {
        this.respuesta=response;
        this.empleados = this.respuesta.entidad;
        this.listarRetornada = 1;
      });
  }
}
