import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Empresa } from "../../Modelo/Cliente/Empresa";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class EmpresaService {
  constructor(private http: HttpClient) {}

  urlEndPoint: string = "http://localhost:8080/lunesdiars/empresas";

  getEmpresas(): Observable<Empresa[]> {
    return this.http.get<Empresa[]>(this.urlEndPoint);
  }
  getEmpresa(codigo: number): Observable<Empresa> {
    return this.http.get<Empresa>(`${this.urlEndPoint}/${codigo}`);
  }
  createEmpresa(empresa: Empresa): Observable<Empresa> {
    return this.http.post<Empresa>(this.urlEndPoint, empresa);
  }

  updateEmpresa(empresa: Empresa): Observable<Empresa> {
    return this.http.put<Empresa>(
      `${this.urlEndPoint}/${empresa.codigo}`,
      empresa
    );
  }
  findByRucOrRazonSocial(empresa: Empresa): Observable<Empresa[]> {
    return this.http.post<Empresa[]>(`${this.urlEndPoint}/parametros`, empresa);
  }
}
