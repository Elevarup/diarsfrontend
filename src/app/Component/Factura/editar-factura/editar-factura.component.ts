import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Response} from "../../../Modelo/Utils/Response";
import { Factura } from 'src/app/Modelo/Factura/Factura';
import { FacturaService } from 'src/app/Service/factura.service';
import {Producto} from "../../../Modelo/Producto/Producto";

@Component({
  selector: 'app-editar-factura',
  templateUrl: './editar-factura.component.html',
  styleUrls: ['./editar-factura.component.css']
})
export class EditarFacturaComponent implements OnInit {

  constructor(
    private activeRouter :ActivatedRoute,
    private facturaService:FacturaService
  ) { }

  titulo:String = 'Editar factura';

  respuesta:Response = new Response();
  factura:Factura = new Factura();

  producto:Producto = new Producto();

  ngOnInit() {
    this.cargarFactura();
  }
  
  cargarFactura():void{
    this.activeRouter.params.subscribe(parametros=>{
      let codigoFactura = parametros["codigo"];
      this.facturaService.getFactura(codigoFactura).subscribe(response=>{
        this.respuesta = response;
        this.factura=this.respuesta.entidad;
      });
    });
  }

}
