import { Component, OnInit } from "@angular/core";
import { FacturaService } from "../../../Service/factura.service";
import { EmpresaService } from "../../../Service/Cliente/empresa.service";
import { Empresa } from "../../../Modelo/Cliente/Empresa";
import { Factura } from "../../../Modelo/Factura/Factura";
import { Temporal } from "../../../Modelo/Utils/Temporal";
import { FACTURAS } from "../../../Modelo/Utils/Arreglo.json";
import { Router, ActivatedRoute } from "@angular/router";
import {Response} from "../../../Modelo/Utils/Response";
import swal from "sweetalert2";

@Component({
  selector: "app-listar-facturas",
  templateUrl: "./listar-facturas.component.html",
  styleUrls: ["./listar-facturas.component.css"]
})
export class ListarFacturasComponent implements OnInit {
  constructor(
    private facturaService: FacturaService,
    private empresaService: EmpresaService,
    private router: Router,
    private route:ActivatedRoute
  ) {}

  ///Variables
  facturas: Factura[];
  temporales: Temporal[];
  empresa: Empresa = new Empresa();
  factura: Factura = new Factura();
  //temporal: Temporal = new Temporal();
  ///Arreglo

  //arreglo: Array[];
  //let arreglo[];

  ////respuesta
  respuesta:Response= new Response();
  arreglo: Array<string>;
  ///contador
  contador: number = 0;

  ngOnInit() {
    this.cargarFacturas();
  }

  cargarFacturas(): void {
    this.facturaService.getFacturas().subscribe(response=>{
      this.respuesta=response;
      this.facturas = this.respuesta.entidad;
    });
  }
  editarFactura(temporal: Temporal) {
    this.router.navigate(["/editarfactura"]);
  }
  eliminarFactura(id_factura:number){
    swal
      .fire({
        title: "¿Seguro que desea eliminar el registro?",
        text: `¿Estás seguro que deseas eliminar cliente la factura con código ${id_factura}?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, eliminar la factura"
      })
      .then(result => {
        if (result.value) {
          this.facturaService.deleteFactura(id_factura).subscribe(response => {
            this.facturas = this.facturas.filter(
              _factura => _factura.codigo !== id_factura
            );
            swal.fire(
              "Eliminado!",
              "La factura fue eliminada  con éxito",
              "success"
            );
            //this.router.navigate(['/listarfacturas']);
          });
        }
    });
  }
}
