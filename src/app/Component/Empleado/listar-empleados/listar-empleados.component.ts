import { Component, OnInit } from "@angular/core";
import { Empleado } from "../../../Modelo/Empleado/Empleado";
import { EmpleadoService } from "../../../Service/empleado.service";
import { Router } from "@angular/router";
import {Response} from "../../../Modelo/Utils/Response";

@Component({
  selector: "app-listar-empleados",
  templateUrl: "./listar-empleados.component.html",
  styleUrls: ["./listar-empleados.component.css"]
})
export class ListarEmpleadosComponent implements OnInit {
  constructor(
    private empleadoService: EmpleadoService,
    private router: Router
  ) {}
  empleados: Empleado[];
  empleado: Empleado = new Empleado();

  respuesta:Response = new Response();

  ngOnInit() {
    this.cargarEmpleados();
  }

  cargarEmpleados(): void {
    this.empleadoService.getEmpleados().subscribe(response => {
      this.respuesta=response;
      this.empleados = this.respuesta.entidad;
    });
  }
  eliminarEmpleado(_empleado: Empleado): void {
    this.empleadoService
      .deleteEmpleado(_empleado.codigo)
      .subscribe(response => {
        alert("Empleado eliminado");
        this.empleados = this.empleados.filter(
          response => response != _empleado
        );
      });
  }
}
