import { Component, OnInit } from '@angular/core';
import {Factura} from '../../../Modelo/Factura/Factura';
import {FacturaService } from '../../../Service/factura.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Response} from '../../../Modelo/Utils/Response';
import {Producto} from '../../../Modelo/Producto/Producto';
import {Empleado} from '../../../Modelo/Empleado/Empleado';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.css']
})
export class DetalleFacturaComponent implements OnInit {

  constructor(
    private router:Router,
    private activeRouter:ActivatedRoute,
    private facturaService:FacturaService
  ) { }

    productos:Producto[];
    factura:Factura = new Factura();
    empleado:Empleado = new Empleado();

    respuesta:Response = new Response;

  ngOnInit() {
    this.cargarFactura();
  }
  cargarFactura(){
    this.activeRouter.params.subscribe(parametros=>{
      let id_factura = parametros['codigo'] 
      this.facturaService.getFactura(id_factura).subscribe(response=>{
        this.respuesta = response;
        ////lista de productos
        this.productos= this.respuesta.entidad.detalles;

        ////factura
        this.factura.codigo=this.respuesta.entidad.codigo;
        this.factura.fecha = this.respuesta.entidad.fecha;
        this.factura.empleado=this.respuesta.entidad.empleado;
        this.factura.monto=this.respuesta.entidad.monto;

       ///empleado
       this.empleado = this.respuesta.entidad.empleado;
        
      });
    });
  }
}
