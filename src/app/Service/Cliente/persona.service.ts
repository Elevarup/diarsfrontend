import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Persona } from "../../Modelo/Cliente/Persona";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PersonaService {
  constructor(private http: HttpClient) {}
  private httpHeaders: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });
  private httpParams: HttpParams;

  urlEndPoint: string = "http://localhost:8080/lunesdiars/personas";

  getPersonas(): Observable<Persona[]> {
    return this.http.get<Persona[]>(this.urlEndPoint);
  }

  getPersona(codigo: number): Observable<Persona> {
    console.log("url:", `${this.urlEndPoint}/${codigo}`);
    return this.http.get<Persona>(`${this.urlEndPoint}/${codigo}`);
  }
  update(persona: Persona): Observable<Persona> {
    return this.http.put<Persona>(
      `${this.urlEndPoint}/${persona.codigo}`,
      persona
    );
  }
  delete(codigo: number): Observable<Persona> {
    console.log("service delete, codigo: ", codigo);
    console.log("delete, url:", `${this.urlEndPoint}/${codigo}`);
    return this.http.delete<Persona>(`${this.urlEndPoint}/${codigo}`);
  }

  create(persona: Persona): Observable<Persona> {
    return this.http.post<Persona>(this.urlEndPoint, persona);
  }
  getPersonaByNombreOrApellidoPaternoOrApellidoMaterno(
    persona: Persona
  ): Observable<Persona[]> {
    return this.http.post<Persona[]>(`${this.urlEndPoint}/parametros`, persona);
  }
}
